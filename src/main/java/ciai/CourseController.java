package ciai;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ciai.model.Course;

@Controller
@RequestMapping(value = "/courses")
public class CourseController {
	
	Course[] courses = {
						new Course(0, "Conceção e Implementação de Aplicações para a Internet"),
						new Course(1, "Segurança de Software"),
						new Course(2,"Interpretação e Compilação de Linguagens"),
						new Course(3,"Introdução à Investigção Operacional")
						};
	
	@RequestMapping(value = "")
	public @ResponseBody List<Course> getCourses(){
		return Arrays.asList(courses);
	}
	
	@RequestMapping(value = "/{id}", method=RequestMethod.GET)
	public @ResponseBody Course getCourse(@PathVariable int id){
		return courses[id];
	}

}
