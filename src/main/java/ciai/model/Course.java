package ciai.model;

public class Course {
	private int id;
	private String name;
	
	public Course(int id, String name){
		this.setId(id);
		this.setName(name);
	}
	
	private int getId(){
		return id;
	}
	
	private void setId(int id){
		this.id = id;
	}
	
	public String getName(){
		return name;
	}
	
	private void setName(String name){
		this.name = name;
	}
	
}
