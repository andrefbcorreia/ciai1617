
var StudentList = React.createClass({
	render: function(){
		return (<div>StudentList blablabla</div>);
	}
});


var StudentDetails = React.createClass({
	render: function(){
		return (<div>Student Detail</div>);
	}
	
});


var StudentApp = React.createClass ({
	render: function() {
		return (
			<div>
				<Link to="/students"></Link>
			</div>
		);
	}
});

var Link = ReactRouter.Link;
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;

ReactDOM.render((
	<Router>
		<Route path="/" component={StudentApp}/>
    	<Route path="/students" component={StudentList}/>
    	<Route path="/students/id" component={StudentDetails}/>
    </Router>
	), document.getElementById('react_content'))	
